const { series, src, dest } = require('gulp');

function bootstrap() {
  const files = [
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/bootstrap/dist/js/bootstrap.min.js'
  ]
  return src(files).pipe(dest('systembuilder/static/npm_pkgs/bootstrap'))
}

function jquery() {
  const files = [
    'node_modules/jquery/dist/jquery.min.js'
  ]
  return src(files).pipe(dest('systembuilder/static/npm_pkgs/jquery'))
}

function rustwebviewer()
{
  const files = [
    'node_modules/rust-web-viewer/rust_web_viewer_bg.wasm',
    'node_modules/rust-web-viewer/rust_web_viewer.js'
  ]
  return src(files).pipe(dest('systembuilder/static/npm_pkgs/rustwebviewer'))
}

exports.default = series(bootstrap, jquery,rustwebviewer)