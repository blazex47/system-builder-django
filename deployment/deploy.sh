 #!/bin/bash

if [ -z "$1" ]
  then
    echo "No argument supplied, please indicate the deployment folder"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No argument supplied, please indicate the docker tag"
    exit 1
fi

#apply
terraform -chdir=$1 apply -auto-approve -var "docker_img=192.168.1.160:5555/webserver:$2" -var "git_commit_hash=123456"