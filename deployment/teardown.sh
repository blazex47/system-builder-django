 #!/bin/bash

if [ -z "$1" ]
  then
    echo "No argument supplied, please indicate the deployment folder"
    exit 1
fi

#destroy
terraform -chdir=$1 destroy -auto-approve
