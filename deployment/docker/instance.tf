terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}

provider "docker" {
  host = "ssh://rancher@192.168.1.31:22"

  registry_auth {
    address = "http://192.168.1.160:5555"
  }
}

data "docker_registry_image" "webserver_img" {
  insecure_skip_verify = true
  name = "192.168.1.160:5555/webserver:0.0.1"
}

# Pulls the image
resource "docker_image" "postgres_img" {
  name = "postgres:13.3-alpine"
}

resource "docker_image" "webserver_img" {
  name = data.docker_registry_image.webserver_img.name
  pull_triggers = [data.docker_registry_image.webserver_img.sha256_digest]
}

# Create a container
resource "docker_container" "postgres" {
  image = docker_image.postgres_img.name
  name  = "postgres_server"
}

resource "docker_container" "webserver" {
  image = docker_image.webserver_img.name
  name  = "webserver_server"

  ports {
    external = 80
    internal = 8000
  }
}
