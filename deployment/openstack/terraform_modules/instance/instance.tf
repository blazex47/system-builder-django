variable "NAME" {
}

variable "IMAGE_NAME" {
}

variable "FLAVOR_NAME" {
}

variable "SEC_GROUP_NAME" {
}

variable "IP_ADDR"{
}

variable "PORT_NUM" {
}

variable "WORK_DIR" {
}

variable "DOCKER_TAG"{
}

data "openstack_networking_floatingip_v2" "myip" {
  address = var.IP_ADDR
}

data "openstack_compute_keypair_v2" "kp" {
  name = "terraform_kp"
}

resource "openstack_compute_secgroup_v2" "sec_group" {
  name = var.SEC_GROUP_NAME
  description = "security group"

  rule {
    ip_protocol = "icmp"
    from_port = "-1"
    to_port = "-1"
    cidr = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port = "22"
    to_port = "22"
    cidr = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port = var.PORT_NUM
    to_port = var.PORT_NUM
    cidr = "0.0.0.0/0"
  }
}

resource "openstack_compute_instance_v2" "basic" {
  name              = var.NAME
  image_name        = var.IMAGE_NAME
  flavor_name       = var.FLAVOR_NAME
  key_pair        = "${data.openstack_compute_keypair_v2.kp.name}"
  security_groups = ["${openstack_compute_secgroup_v2.sec_group.name}"]
  user_data       = file("${var.WORK_DIR}/../packer/cloud_config.yml")
  config_drive    = true

  network {
    name = "homelab-network"
  }
}

resource "openstack_compute_floatingip_associate_v2" "myip" {
  floating_ip = "${data.openstack_networking_floatingip_v2.myip.address}"
  instance_id = "${openstack_compute_instance_v2.basic.id}"
}

resource "null_resource" "provision" {
  depends_on = [openstack_compute_floatingip_associate_v2.myip]
  connection {
    type        = "ssh"
    user        = "rancher"
    timeout     = "15m"
    host        = "${data.openstack_networking_floatingip_v2.myip.address}"
    private_key = file("${var.WORK_DIR}/../../../terraform_kp.pem")
  }
  
  provisioner "remote-exec" {
    script = "${var.WORK_DIR}/../packer/setup.sh"
  }
}

# generate inventory file for Ansible
resource "local_file" "inventory_txt" {
  content = templatefile("${var.WORK_DIR}/../templates/inventory.tpl",
    {
      rancheros_servers_ip = openstack_compute_floatingip_associate_v2.myip.*.floating_ip
    }
  )
  file_permission = "0600"
  filename = "${var.WORK_DIR}/ansible/inventory.txt"
}