module "instances" {
  source            = "../../terraform_modules/instance"
  NAME              = "ros_dev_1c1g"
  IMAGE_NAME        = "rancheros"
  FLAVOR_NAME       = "m1.micro"
  SEC_GROUP_NAME    = "sec_group_1"
  IP_ADDR           = "192.168.1.30"
  PORT_NUM          = "80"
  WORK_DIR          = path.module
  DOCKER_TAG        = var.docker_tag
}

variable "docker_tag" {
  description = "docker tag for deployment"
  type        = string
  default     = "latest"
}