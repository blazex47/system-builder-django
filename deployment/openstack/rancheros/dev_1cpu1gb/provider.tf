provider "openstack" {
  user_name   = "terraform"
  tenant_name = "admin"
  password    = "pwd"
  auth_url    = "http://192.168.1.59:5000/v3"
  region      = "microstack"
}