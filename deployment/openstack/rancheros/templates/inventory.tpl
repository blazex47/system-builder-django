[rancheros_hosts]
%{ for ip in rancheros_servers_ip ~}
${ip} ansible_user=rancher ansible_ssh_common_args='-o StrictHostKeyChecking=no' ansible_ssh_private_key_file=../terraform_kp.pem ansible_connection=ssh ansible_python_interpreter=/usr/bin/python3
%{ endfor ~}
