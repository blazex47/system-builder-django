module "instances" {
  source            = "../../terraform_modules/instance"
  NAME              = "ros_dev_2c2g"
  IMAGE_NAME        = "rancheros"
  FLAVOR_NAME       = "m1.s-medium"
  SEC_GROUP_NAME    = "sec_group_2"
  IP_ADDR           = "192.168.1.31"
  PORT_NUM          = "80"
  WORK_DIR          = path.module
  DOCKER_TAG        = var.docker_tag
}

variable "docker_tag" {
  description = "docker tag for deployment"
  type        = string
  default     = "latest"
}