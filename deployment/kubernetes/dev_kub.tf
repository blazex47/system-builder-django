terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}

provider "kubernetes" {
  config_path    = "./kube_config.yml"
  #config_context = "my-context"
}

variable "docker_img" {
  description = "docker img for deployment"
  type        = string
  default     = "blazex47/webserver:latest"
}

variable "git_commit_hash" {
  description = "git commit hash for deployment"
  type        = string
  default     = "000000"  
}

resource "kubernetes_namespace" "test" {
  metadata {
    name = "webserver-${var.git_commit_hash}"
  }
}

resource "kubernetes_deployment" "test" {
  metadata {
    name      = "webserver-django"
    namespace = kubernetes_namespace.test.metadata.0.name
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "MyTestApp"
      }
    }
    template {
      metadata {
        labels = {
          app = "MyTestApp"
        }
      }
      spec {
        container {
          image = "postgres:13.3-alpine"
          name  = "postgres-container"
          env {
            name = "POSTGRES_DB"
            value = "postgres"
          }
          env {
            name = "POSTGRES_USER"
            value = "postgres"
          }
          env {
            name = "POSTGRES_PASSWORD"
            value = "postgres"
          }
          port {
            container_port = 8080
          }
        }
        container {
          image = "${var.docker_img}"
          name  = "webserver-container"
          port {
            container_port = 8000
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "test" {
  metadata {
    name      = "webserver-django"
    namespace = kubernetes_namespace.test.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.test.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      #node_port   = 30201
      port        = 8000
      target_port = 8000
    }
  }
}

# generate env file for Gitlab CI
resource "local_file" "deploy_env" {
  content = templatefile("${path.module}/templates/deploy.tpl",
    {
      dynamic_url = kubernetes_service.test.spec.0.port.0.node_port
    }
  )
  file_permission = "0600"
  filename = "${path.module}/deploy.env"
}