FROM node:16 as nodebuild

WORKDIR /pre

COPY ./systembuilder ./systembuilder
COPY ./gulpfile.js ./
COPY ./package-lock.json ./
COPY ./package.json ./

RUN npm install

FROM python:3 as final

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED=1

WORKDIR /code

COPY --from=nodebuild /pre/systembuilder ./systembuilder

COPY ./pages ./pages
COPY ./templates ./templates
COPY ./requirements.txt ./
COPY ./manage.py ./

RUN pip install -r requirements.txt
RUN pip install Pillow

RUN python manage.py collectstatic --noinput
#RUN python manage.py makemigrations
#RUN python manage.py migrate

CMD ["python","manage.py","runserver","0.0.0.0:8000"]