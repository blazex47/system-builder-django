const systembuilder = Vue.createApp({
    // data, functions
    delimiters: ['[[', ']]'],
    data(){
        return {
            fullscreen: false,
            parts_list: []
        }
    },
    created() 
    {
        /* Standard syntax */
        document.addEventListener("fullscreenchange", this.toggleFullscrenFlag);
        /* Firefox */
        document.addEventListener("mozfullscreenchange", this.toggleFullscrenFlag);
        /* Chrome, Safari and Opera */
        document.addEventListener("webkitfullscreenchange", this.toggleFullscrenFlag);
        /* IE / Edge */
        document.addEventListener("msfullscreenchange", this.toggleFullscrenFlag);

        window.addEventListener('resize', this.onWindowResize);
    },
    mounted()
    {
      fetch('http://localhost:3000/parts_list')
      .then(response => response.json())
      .then(data => this.parts_list = data)
      .catch(err => console.log(err.message));
    },
    destroyed() 
    {
        /* Standard syntax */
        document.removeEventListener("fullscreenchange", this.toggleFullscrenFlag);
        /* Firefox */
        document.removeEventListener("mozfullscreenchange", this.toggleFullscrenFlag);
        /* Chrome, Safari and Opera */
        document.removeEventListener("webkitfullscreenchange", this.toggleFullscrenFlag);
        /* IE / Edge */
        document.removeEventListener("msfullscreenchange", this.toggleFullscrenFlag);

        window.removeEventListener('resize', this.onWindowResize);
    },
    methods:
    {
        onWindowResize()
        {
          let parts_elem = this.$refs.partstab;

          let parts_style = window.getComputedStyle(parts_elem);

          if (parts_style.getPropertyValue('display') == "none" && parts_elem.classList.contains("active")) 
          {
            let parts_list = document.getElementById("parts-list");

            parts_elem.classList.remove("active");
            parts_list.classList.remove("active","show");

            let reviews_elem = this.$refs.reviewstab;
            let reviews = document.getElementById("reviews");
            
            reviews_elem.classList.add("active");
            reviews.classList.add("active","show");
          }          
        },
        changeResolution(event,width,height)
        {
            let elem = event.currentTarget;

            var button = document.getElementById("resolution-button");
            button.innerHTML = elem.innerHTML;

            var canvas = document.getElementById("canvas");
            canvas.width = width;
            canvas.height = height;
        },
        toggleFullscrenFlag()
        {
            this.fullscreen = !this.fullscreen;
        },
        toggleFullscreen()
        {
            let element = document.getElementById("visualizer");

            if (this.fullscreen) {
              if (document.exitFullscreen) {
                document.exitFullscreen();
              } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
              } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
              } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
              }
            } else {
              if (element.requestFullscreen) {
                element.requestFullscreen();
              } else if (element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
              } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
              } else if (element.msRequestFullscreen) {
                // IE11
                element.msRequestFullscreen();
              }
            }
        }
    }
})

systembuilder.mount('#systembuilder')